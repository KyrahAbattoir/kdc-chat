Mod - KDC Chat
==============

Minetest Version: 5.0.0 and newer

Dependencies: default
Optional dependencies: bucket, mesecraft_bucket, 3d_armor

This mods modifies chat and limit its range.

Chat by default is limited to 10 meters (20 meters with direct line of sight)

/me is limited to a 10 meters range.

/whisper is limited to 5 meters (10 meters with direct line of sight)

/shout is limited to 50 meters (100 meters with direct line of sight)

### Optional dependencies

bucket or mesecraft_bucket: required to craft the radio (when placed, it functions as a chat range extender).

3d_armor: armor pieces in the "muffling" group will prevent the wearer from talking.
