
function kdc_chat.on_emote(sender_name, message, do_tests)

    if message == "" then
        return false
    end

    do_tests = do_tests or true;

    sender_name = minetest.strip_colors(sender_name)
    message = minetest.strip_colors(message)

    local sender = minetest.get_player_by_name(sender_name)

    message = string.format("*%s %s*", sender_name, message)
    message = minetest.colorize(kdc_chat.emote_color, message)

    local sender_pos = sender:get_pos()

    for _, target in ipairs(minetest.get_connected_players()) do

        if do_tests then
            local target_pos = target:get_pos()
            if sender ~= target then
                if vector.distance(sender_pos, target_pos) > kdc_chat.emote_dist then
                    goto next_emote_viewer end
            end
        end

        minetest.chat_send_player(target:get_player_name(), message)
        ::next_emote_viewer::
    end

    return true
end
