function kdc_chat.inRadioRange(player, radius)
    local player_pos = player:get_pos()
    return not (minetest.find_node_near(player_pos, radius, kdc_chat.radio, true) == nil)
end

kdc_chat.enable_muffling = minetest.get_modpath("3d_armor")

function kdc_chat.isMuffled(player)
    --this feature requires the 3d_armor mod sooo...
    if not kdc_chat.enable_muffling then
        return false
    end

    local name, armor_inv = armor:get_valid_player(player)
	if not name then
        return false
	end

    for i=1, armor_inv:get_size("armor") do
        local stack = armor_inv:get_stack("armor", i)
        if minetest.get_item_group(stack:get_name(), "muffling") > 0 then
            return true
        end
	end
    return false
end

function kdc_chat.doBroadcast(sender_name, base_range, sighted_range, message)

    local sender = minetest.get_player_by_name(sender_name)
    local sender_eye = sender:get_properties().eye_height
    local sender_pos = sender:get_pos() + vector.new(0, sender_eye, 0)

    --If the sender is muffled, send an emote instead.
    if kdc_chat.isMuffled(sender) then
        kdc_chat.on_emote(sender_name, "mumbles something unintelligible.")
        return
    end

    --the sender end of radio (base_range)
    local sender_has_radio = kdc_chat.inRadioRange(sender, base_range)

    local alone = true

    for _, target in ipairs(minetest.get_connected_players()) do

        --we always hear our own chat
        if sender == target then
            kdc_chat.doSend(target, message)
            goto next_listener
        end

        local target_eye = target:get_properties().eye_height
        local target_pos = target:get_pos() + vector.new(0, target_eye, 0)
        local distance = vector.distance(sender_pos, target_pos);

        --close enough to hear the message unaided.
        if distance <= base_range then
            alone = false
            kdc_chat.doSend(target, message)
            goto next_listener
        end

        --in visual and sighted range
        local sighted = minetest.line_of_sight(sender_pos, target_pos)
        if (distance <= sighted_range) and sighted then
            alone = false
            kdc_chat.doSend(target, message)
            goto next_listener
        end

        --the receiving end of radio (kdc_chat.radio_dist)
        local target_has_radio = kdc_chat.inRadioRange(target, kdc_chat.radio_dist)
        if sender_has_radio and target_has_radio then
            alone = false
            kdc_chat.doRadio(target, message)
            goto next_listener
        end

        --finally process chats converted to mumbles
        if distance <= sighted_range then
            alone = false
            kdc_chat.on_emote(sender_name, "mumbles.", false)
        end

        ::next_listener::
    end

    if(alone) then
        minetest.chat_send_player(sender_name, kdc_chat.alone)
    end
end

function kdc_chat.doRadio(target, message)
    local radio_header = string.format("[%s%s%s] ",  minetest.get_color_escape_sequence(kdc_chat.radio_color),
                                                    kdc_chat.radio_header,
                                                    minetest.get_color_escape_sequence("#FFFFFF") )
    kdc_chat.doSend(target, radio_header..message)
end

function kdc_chat.doSend(target, message)
    minetest.chat_send_player(target:get_player_name(), message)
end

