function kdc_chat.on_whisper(sender_name, message)

    if message == "" then
        return false
    end

    sender_name = minetest.strip_colors(sender_name)
    message = minetest.strip_colors(message)

    local colored_sender = minetest.colorize(kdc_chat.username_color, sender_name)
    message = string.format("%s whispers, \"%s\"", colored_sender, message)

    kdc_chat.doBroadcast(sender_name, kdc_chat.whisper_short, kdc_chat.whisper_long, message)

    return true
end

function kdc_chat.on_shout(sender_name, message)

    if message == "" then
        return false
    end

    sender_name = minetest.strip_colors(sender_name)
    message = minetest.strip_colors(message)

    local colored_sender = minetest.colorize(kdc_chat.username_color, sender_name)
    message = string.format("%s shouts, \"%s\"", colored_sender, string.upper(message))

    kdc_chat.doBroadcast(sender_name, kdc_chat.shout_short, kdc_chat.shout_long, message)

    return true
end

function kdc_chat.on_chat(sender_name, message)
    sender_name = minetest.strip_colors(sender_name)
    message = minetest.strip_colors(message)

    --fancy formatting
    local action = "says";
    local punctuation = string.sub(message, -1)
    if(punctuation == "?") then
        action = "asks"
    elseif(punctuation == "!") then
        action = "exclaims"
    elseif(punctuation ~= ".") then
        message = message .. "."
    end

    local colored_sender = minetest.colorize(kdc_chat.username_color, sender_name)
    message = string.format("%s %s, \"%s\"", colored_sender, action, message)

    kdc_chat.doBroadcast(sender_name, kdc_chat.chat_short, kdc_chat.chat_long, message)

    return true
end
