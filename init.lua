kdc_chat = {}
kdc_chat.modname = minetest.get_current_modname()
kdc_chat.modpath = minetest.get_modpath(kdc_chat.modname)
kdc_chat.radio   = kdc_chat.modname..":radio"

dofile(kdc_chat.modpath.."/chat.lua")
dofile(kdc_chat.modpath.."/emote.lua")
dofile(kdc_chat.modpath.."/internal.lua")
dofile(kdc_chat.modpath.."/radio.lua")

kdc_chat.chat_short     = 20
kdc_chat.chat_long      = 40
kdc_chat.emote_color    = "#555555"
kdc_chat.emote_dist     = 10
kdc_chat.radio_color    = "#AAAAFF"
kdc_chat.radio_dist     = 10    --radios have a limited "loudness" when they emit messages.
kdc_chat.radio_header   = "RADIO"
kdc_chat.shout_short    = 50
kdc_chat.shout_long     = 100
kdc_chat.username_color = "#FF8800"
kdc_chat.whisper_short  = 5
kdc_chat.whisper_long   = 10

kdc_chat.emote_description      = "to emote to nearby players. Visible up to a range of %d block."
kdc_chat.whisper_description    = "To talk very quietly."
kdc_chat.shout_description      = "To shout very loudly."
kdc_chat.range_description      = " Can be heard up to %d blocks away and up to %d blocks in direct line of sight."

kdc_chat.alone = "You talk, but no one can hear your screams."

--Since we are taking over /me.
minetest.unregister_chatcommand("me")

minetest.register_chatcommand("me", {
    description = string.format(kdc_chat.emote_description, kdc_chat.emote_dist),
    params = "<emote>",
    func = function(name, param)
        kdc_chat.on_emote(name, param)
    end
})
minetest.register_chatcommand("em", {
    description = string.format(kdc_chat.emote_description, kdc_chat.emote_dist),
    params = "<emote>",
    func = function(name, param)
        return kdc_chat.on_emote(name, param)
    end
})

minetest.register_chatcommand("w", {
    description = string.format(kdc_chat.whisper_description..kdc_chat.range_description,
                                kdc_chat.whisper_short, kdc_chat.whisper_long),
    params = "<message>",
    func = function(name, param)
        return kdc_chat.on_whisper(name, param)
    end
})
minetest.register_chatcommand("whisper", {
    description = string.format(kdc_chat.whisper_description..kdc_chat.range_description,
                                kdc_chat.whisper_short, kdc_chat.whisper_long),
    params = "<message>",
    func = function(name, param)
        return kdc_chat.on_whisper(name, param)
    end
})

minetest.register_chatcommand("s", {
    description = string.format(kdc_chat.shout_description..kdc_chat.range_description,
                                kdc_chat.shout_short, kdc_chat.shout_long),
    params = "<message>",
    func = function(name, param)
        return kdc_chat.on_shout(name, param)
    end
})
minetest.register_chatcommand("shout", {
    description = string.format(kdc_chat.shout_description..kdc_chat.range_description,
                                kdc_chat.shout_short, kdc_chat.shout_long),
    params = "<message>",
    func = function(name, param)
        return kdc_chat.on_shout(name, param)
    end
})

minetest.register_on_chat_message(function(name, message)
    return kdc_chat.on_chat(name, message)
end)
