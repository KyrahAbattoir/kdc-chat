minetest.register_node(kdc_chat.radio, {
    description = "Shortwave radio",
    tiles = {
                "radio_top.png", "radio_back.png",   --top/bottom
                "radio_sides.png", "radio_sides.png",--side/side
                "radio_back.png", "radio_front.png"  --back/front
            },
    paramtype2 = "facedir",
    is_ground_content = false,
    groups = {choppy=3},
    sounds = default.node_sound_wood_defaults(),
})

if minetest.get_modpath("mesecraft_bucket") then
    minetest.register_craft({
        output = kdc_chat.radio,
        recipe = {
            {'','',"default:mese_crystal_fragment"},
            {"group:wood","mesecraft_bucket:bucket_empty","group:wood"},
            {"group:wood", "default:copper_ingot","group:wood"},
        }
    })
end

if minetest.get_modpath("bucket") then
    minetest.register_craft({
        output = kdc_chat.radio,
        recipe = {
            {'','',"default:mese_crystal_fragment"},
            {"group:wood", "bucket:bucket_empty","group:wood" },
            {"group:wood", "default:copper_ingot","group:wood" },
        }
    })
end

minetest.register_craft({
    type = "fuel",
    recipe = kdc_chat.radio ,
    burntime = 16
})
